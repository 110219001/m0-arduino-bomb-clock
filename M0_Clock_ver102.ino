#include <stdio.h>
#include <DS1302.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F,16,2); 
const int buzzer = 8; //蜂鳴器
const int inPin = 10; //正確引線
const int inPin_2 =11;//錯誤引線
const int btnShowEventTime = 9; //　顯示定時時間
int pin10_DV=0;       //pin10的digital value
int pin11_DV=0;       //pin11的digital value
int btnShowEventTime_DV=0;
namespace {

const int kCePin   = 5;  // Chip Enable
const int kIoPin   = 6;  // Input/Output
const int kSclkPin = 7;  // Serial Clock

DS1302 rtc(kCePin, kIoPin, kSclkPin);//建立rtc物件

String dayAsString(const Time::Day day) {
  switch (day) {
    case Time::kSunday: return "Sunday";
    case Time::kMonday: return "Monday";
    case Time::kTuesday: return "Tuesday";
    case Time::kWednesday: return "Wednesday";
    case Time::kThursday: return "Thursday";
    case Time::kFriday: return "Friday";
    case Time::kSaturday: return "Saturday";
      }
  return "(unknown day)";
  }
void printTime() {
  Time t = rtc.time();
  const String day = dayAsString(t.day);
  char buf[50];
  snprintf(buf, sizeof(buf), "%s %04d-%02d-%02d %02d:%02d:%02d",
           day.c_str(),
           t.yr, t.mon, t.date,
           t.hr, t.min, t.sec);
  Serial.println(buf);
  }
} //namespace ending

void setup()
{
  Serial.begin(9600);
  pinMode(buzzer,OUTPUT);//蜂鳴器 pin8
  pinMode(inPin,INPUT); //正確引線 pin10
  pinMode(inPin_2,INPUT);//錯誤引線 pin11
  pinMode(btnShowEventTime,INPUT);
  lcd.init();            // 初始化lcd 
  lcd.backlight();
  lcd.print("Present By 01&07");
  lcd.setCursor(0,1);
  lcd.print("copyright reserved");
  
  
  rtc.writeProtect(false); //初始化rct
  rtc.halt(false);
  Time t(2015, 1, 13, 1, 38, 00, Time::kSunday);
  rtc.time(t);
  rtc.writeProtect(true);
}

void loop()
{
  Time tEvent(2015,1,13,1,38,5,Time::kSunday);//事件發生時間(鬧鐘時間)
  printTime();//serial print
  delay(1000);
  Time tCurrent=rtc.time();//tCurrent物件裡面存放目前時間，每秒更新一次，與rtc同步
  btnShowEventTime_DV=digitalRead(btnShowEventTime);
  switch(btnShowEventTime_DV){
  case HIGH:
    lcd.clear();
    lcd.print(tEvent.yr);
    lcd.setCursor(4,0);
    lcd.print("/");
    if(tEvent.mon>9){
    lcd.setCursor(5,0);
    lcd.print(tEvent.mon);
    }
    else if(tEvent.mon<10){
    lcd.setCursor(6,0);
    lcd.print(tEvent.mon);
    }
    lcd.setCursor(7,0);
    lcd.print("/");
    lcd.setCursor(8,0);
    lcd.print(tEvent.date);
    if(tEvent.hr>9){
    lcd.setCursor(4,1);
    lcd.print(tEvent.hr);
    }
    else if(tEvent.hr<10){
    lcd.setCursor(5,1);
    lcd.print(tEvent.hr);
    }
    lcd.setCursor(6,1);
    lcd.print(":");
    lcd.setCursor(7,1);
    lcd.print(tEvent.min);
    lcd.setCursor(9,1);
    lcd.print(":");
    if(tEvent.sec>9){
    lcd.setCursor(10,1);
    lcd.print(tEvent.sec);
    }
    else if(tEvent.sec<10){
    lcd.setCursor(11,1);
    lcd.print(tEvent.sec);
    }
    break;
  case LOW:
    lcd.clear();
    lcd.print(tCurrent.yr);
    lcd.setCursor(4,0);
    lcd.print("/");
    if(tCurrent.mon>9){
    lcd.setCursor(5,0);
    lcd.print(tCurrent.mon);
    }
    else if(tCurrent.mon<10){
    lcd.setCursor(6,0);
    lcd.print(tCurrent.mon);
    }
    lcd.setCursor(7,0);
    lcd.print("/");
    lcd.setCursor(8,0);
    lcd.print(tCurrent.date);
    if(tCurrent.hr>9){
    lcd.setCursor(4,1);
    lcd.print(tCurrent.hr);
    }
    else if(tCurrent.hr<10){
    lcd.setCursor(5,1);
    lcd.print(tCurrent.hr);
    }
    lcd.setCursor(6,1);
    lcd.print(":");
    lcd.setCursor(7,1);
    lcd.print(tCurrent.min);
    lcd.setCursor(9,1);
    lcd.print(":");
    if(tCurrent.sec>9){
    lcd.setCursor(10,1);
    lcd.print(tCurrent.sec);
    }
    else if(tCurrent.sec<10){
    lcd.setCursor(11,1);
    lcd.print(tCurrent.sec);
    }
    break;
  }
  if(tEvent==tCurrent){  //鬧鐘事件發生;蜂鳴器作動
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Explode in ...");
    
    for(int j=0;j<9;j++){
      
      lcd.setCursor(5,1);
      lcd.print(9-j);
      
      for(int i=0;i<10;i++){
        tone(buzzer,1000);
        delay(50);
        tone(buzzer,500);
        delay(50);
       }
      pin10_DV=digitalRead(inPin);  //讀取10引線解除情況
      pin11_DV=digitalRead(inPin_2);//讀取11引線解除情況
      Serial.print(pin10_DV);
      noTone(buzzer);
      delay(2000);
      if(pin10_DV==LOW && pin11_DV==HIGH){//引線以拔除
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Bomb Defused");
        delay(2000);
        break;
      }
    }
  } //鬧鐘事件結束
 
}
